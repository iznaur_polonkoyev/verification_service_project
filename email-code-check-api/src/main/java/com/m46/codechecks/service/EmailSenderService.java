package com.m46.codechecks.service;

import com.m46.codechecks.config.EmailCodeCheckProps;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Date;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmailSenderService {

    private final EmailCodeCheckProps verificationProps;
    private final JavaMailSender javaMailSender;

    public void sendVerificationEmail(String email, String customerName, String code) {

        sendEmail(email, customerName, code, verificationProps.getEmailTemplateId());
    }

    public void sendEmail(String email, String customerName, String code, Long templateId) {

        String from = "dikiystore@gmail.com";
        String message = "Здравствуйте "+customerName+" ваш код активации: "+code;
        String subject = "Код подтверждения";
//
        MimeMessagePreparator preparator = new MimeMessagePreparator() {
            public void prepare(MimeMessage mimeMessage) throws MessagingException, UnsupportedEncodingException {
                mimeMessage.setFrom(new InternetAddress(from, "DikiyStore"));
                mimeMessage.setContent(message, "text/html; charset=UTF-8");
                mimeMessage.setSubject(subject, "UTF-8");
                mimeMessage.setSentDate(new Date());
                mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(email));

                mimeMessage.setHeader("Content-Type","text/html; charset=UTF-8");

            }
        };

        try {
            javaMailSender.send(preparator);
            log.info("Mail sent to {} ", email);
        } catch (Throwable e) {
            log.error("Can't send mail, Exception: {}", e.getMessage());
        }


    }

}
