package com.m46.codechecks.dto;

import lombok.Data;

@Data

public class CaptchaRequestDto {

    private String questionId;
    private String answer;
    private Integer personId;
}
