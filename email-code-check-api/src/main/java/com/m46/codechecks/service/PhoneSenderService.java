package com.m46.codechecks.service;

import com.m46.codechecks.config.EmailCodeCheckProps;
import com.m46.codechecks.smsApi.Smsc;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class PhoneSenderService {

    private final EmailCodeCheckProps verificationProps;
    private final JavaMailSender javaMailSender;

    public void sendVerificationPhone(String email, String customerName, String code) {

        sendPhone(email, customerName, code, verificationProps.getEmailTemplateId());
    }

    public void sendPhone(String email, String customerName, String code, Long templateId) {

        Smsc sd= new Smsc();

        sd.send_sms(email, "Здравствуйте "+customerName+"! Ваш код активации: "+code, 1, "", "", 0, "", "");
        sd.get_balance();

    }
}
