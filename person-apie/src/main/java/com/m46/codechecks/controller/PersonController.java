package com.m46.codechecks.controller;

import com.m46.codechecks.dto.CaptchaRequestDto;
import com.m46.codechecks.dto.PersonDto;
import com.m46.codechecks.dto.EmailRequestDto;
import com.m46.codechecks.service.PersonService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/person")
public class PersonController {

    private final PersonService personService;

    @PostMapping(path = "/create")
    private ResponseEntity<Object> createPerson(@RequestBody PersonDto dto) {
        log.info("Checking for this one: '{}'", dto.getEmail());
        return ResponseEntity.ok(personService.createPerson(dto));
    }

    @PostMapping(path = "/verify")
    private ResponseEntity<Object> emailVerifyCheck(@RequestBody EmailRequestDto dto) {

        return ResponseEntity.ok(personService.verifyEmail(dto));

    }

    @PostMapping(path = "/question")
    private ResponseEntity<?> captchaVerifyCheck(@RequestBody CaptchaRequestDto dto) {

        personService.verifyCaptcha(dto);
        personService.updateProductStatus(dto);
        return ResponseEntity.ok(dto);
    }
}
