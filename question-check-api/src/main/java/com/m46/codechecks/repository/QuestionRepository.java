package com.m46.codechecks.repository;

import com.m46.codechecks.entity.QuestionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface QuestionRepository extends JpaRepository<QuestionEntity, Integer> {

    @Query(value = "select * from captcha order by random() LIMIT 1", nativeQuery = true)
    QuestionEntity getContactAndPerson();

//    Optional<QuestionEntity> findById(int questionId);
}
