package com.m46.codechecks.service;

import com.m46.codechecks.dto.CaptchaRequestDto;
import com.m46.codechecks.dto.PersonDto;
import com.m46.codechecks.dto.EmailRequestDto;
import com.m46.codechecks.dto.ProductDto;
import com.m46.codechecks.entity.PersonEntity;
import com.m46.codechecks.entity.ProductEntity;
import com.m46.codechecks.repository.PersonRepository;
import com.m46.codechecks.repository.ProductRepository;
import com.m46.codechecks.utils.RestTemplateUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class PersonService {

    private final PersonRepository personRepository;
    private final ProductRepository productRepository;
    private final ModelMapper modelMapper;
    private final ProductService productService;
    private final RestTemplateUtil restTemplateUtil;
    private PersonEntity personEntity;

    public Object createPerson(PersonDto dto) {

        personEntity = personRepository
                .save(modelMapper.map(dto, PersonEntity.class));
        productService.createProducts(dto.getProducts(), personEntity.getId());
        restTemplateUtil.emailSend(personEntity);
        System.out.println("FIASCO");
        return personEntity;

    }

    public Object verifyEmail(EmailRequestDto dto) {
        restTemplateUtil.emailTemplate(dto);
        return restTemplateUtil.captchaSend();
    }

    public Object verifyCaptcha(CaptchaRequestDto dto) {
        return restTemplateUtil.captchaTemplate(dto);
    }

    public void updateProductStatus(CaptchaRequestDto dto) {
        ProductEntity entity = productRepository.findByPersonId(dto.getPersonId());

        entity.setStatus("DONE");

        productRepository.saveAndFlush(entity);

        log.info("Product entity: {}", entity);

    }
}


