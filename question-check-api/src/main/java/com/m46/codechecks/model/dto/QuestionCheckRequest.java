package com.m46.codechecks.model.dto;

import lombok.Data;

@Data
public class QuestionCheckRequest {

    private int questionId;
    private String answer;
}
