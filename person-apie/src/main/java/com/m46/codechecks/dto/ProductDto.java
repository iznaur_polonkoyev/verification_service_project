package com.m46.codechecks.dto;

import lombok.Data;

@Data

public class ProductDto {

    private Integer id;
    private String name;
    private String status;
    private Integer personId;
}
