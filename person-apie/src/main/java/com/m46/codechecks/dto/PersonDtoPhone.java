package com.m46.codechecks.dto;

import lombok.Data;

import java.util.List;

@Data
public class PersonDtoPhone {

    private Integer id;
    private String phone;
    private String name;
    private String surname;
    private String patronymic;
    private List<ProductDto> products;
}
