package com.m46.codechecks.repository;


import com.m46.codechecks.entity.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends JpaRepository<PersonEntity, Integer> {

    PersonEntity findByEmail(String email);

}

