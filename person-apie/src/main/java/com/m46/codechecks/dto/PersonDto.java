package com.m46.codechecks.dto;

import lombok.Data;

import java.util.List;

@Data
public class PersonDto {

    private Integer id;
    private String email;
    private String name;
    private String surname;
    private String patronymic;
    private List<ProductDto> products;
}
