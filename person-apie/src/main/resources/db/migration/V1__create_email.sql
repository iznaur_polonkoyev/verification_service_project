CREATE TABLE persons
(
    id         SERIAL PRIMARY KEY,
    email      CHARACTER VARYING(128) NOT NULL,
    name       CHARACTER VARYING(128) NOT NULL,
    surname    CHARACTER VARYING(128) NOT NULL,
    patronymic CHARACTER VARYING(128) NOT NULL
);

CREATE TABLE products
(
    id        SERIAL PRIMARY KEY,
    name      CHARACTER VARYING(128) NOT NULL,
    status    CHARACTER VARYING(128),
    person_id INTEGER references persons (id)
);

CREATE TABLE captcha
(
    id       SERIAL PRIMARY KEY,
    question CHARACTER VARYING(128) NOT NULL,
    answer   CHARACTER VARYING(128) NOT NULL
);
insert into captcha (question, answer) values ('Пятый день недели','Пятница');
insert into captcha (question, answer) values ('Сколько дней в году?','365');
insert into captcha (question, answer) values ('Сколько планет?','8');

CREATE TABLE email_verifications
(
    verification_id     SERIAL PRIMARY KEY,
    email               CHARACTER VARYING(128)                    NOT NULL,
    created_at          TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
    verification_code   CHARACTER VARYING(16)                     NOT NULL,
    verification_status CHARACTER VARYING(16),
    message             CHARACTER VARYING(256)
)
