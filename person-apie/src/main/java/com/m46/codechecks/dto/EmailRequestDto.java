package com.m46.codechecks.dto;

import lombok.Data;

@Data

public class EmailRequestDto {

    private String email;
    private String verificationCode;
}
