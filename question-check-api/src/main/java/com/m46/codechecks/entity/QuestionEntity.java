package com.m46.codechecks.entity;

import lombok.Data;

import javax.persistence.*;


@Data
@Entity
@Table(name = "captcha")
public class QuestionEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "question", nullable = false)
    private String question;

    @Column(name = "answer", nullable = false)
    private String answer;

}
