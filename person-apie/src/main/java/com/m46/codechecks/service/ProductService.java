package com.m46.codechecks.service;

import com.m46.codechecks.dto.ProductDto;
import com.m46.codechecks.entity.ProductEntity;
import com.m46.codechecks.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;
    private final ModelMapper modelMapper;
    private final RestTemplate restTemplate;



    void createProducts(List<ProductDto> dtos,int personId) {
            dtos.forEach(item->item.setPersonId(personId));
        List<ProductEntity> items = dtos.stream()
                .peek(item -> item.setStatus("NEW"))
                .map(item -> modelMapper.map(item, ProductEntity.class))
                .collect(Collectors.toList());
        productRepository.saveAll(items);


    }
}
