package com.m46.codechecks.service;


import com.m46.codechecks.entity.QuestionEntity;
import com.m46.codechecks.model.dto.QuestionCheckRequest;
import com.m46.codechecks.model.dto.QuestionDto;
import com.m46.codechecks.repository.QuestionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class QuestionService {

    private final QuestionRepository questionRepository;
    private final ModelMapper modelMapper;


    public QuestionDto getQuestion() {
        QuestionEntity entities = questionRepository.getContactAndPerson();
        return modelMapper.map(entities, QuestionDto.class);

    }

    public Object postAnswer(QuestionCheckRequest answer) {
        Optional<QuestionEntity> questionEntity = questionRepository.findById(answer.getQuestionId());
        if (Objects.equals(answer.getAnswer(), questionEntity.get().getAnswer())) {
            System.out.println("Вопрос совпадает!");
            return answer;
        }
        return new Object();
    }
}
