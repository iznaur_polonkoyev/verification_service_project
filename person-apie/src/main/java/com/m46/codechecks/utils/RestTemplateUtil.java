package com.m46.codechecks.utils;

import com.m46.codechecks.dto.CaptchaRequestDto;
import com.m46.codechecks.dto.EmailRequestDto;
import com.m46.codechecks.entity.PersonEntity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor
public class RestTemplateUtil {

    public void emailSend(PersonEntity personEntity) {
        System.out.println(personEntity);
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        Map map = new HashMap<String, String>();

        map.put("Content-Type", "application/json");
        headers.setAll(map);

        Map params = new HashMap();
        params.put("email", personEntity.getEmail());
        params.put("name", personEntity.getName() + " " + personEntity.getSurname() + " " + personEntity.getPatronymic());
        HttpEntity<?> request = new HttpEntity<>(params, headers);
        String url = "http://164.90.230.207:8082/email/send";

        ResponseEntity<?> response = new RestTemplate().postForEntity(url, request, String.class);

    }

    public Object captchaSend() {

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        Map map = new HashMap<String, String>();

        map.put("Content-Type", "application/json");
        headers.setAll(map);

        Map params = new HashMap();
        HttpEntity<?> request = new HttpEntity<>(params, headers);
        String url = "http://164.90.230.207:8081/captcha/question";

        ResponseEntity<?> response = new RestTemplate().getForEntity(url, Object.class);
        return response.getBody();
    }

    public Object emailTemplate(EmailRequestDto emailrequestDto) {
        System.out.println("emailtemplate" + emailrequestDto.toString());

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        Map map = new HashMap<String, String>();

        map.put("Content-Type", "application/json");
        headers.setAll(map);

        Map params = new HashMap();
        params.put("email", emailrequestDto.getEmail());
        params.put("verificationCode", emailrequestDto.getVerificationCode());

        HttpEntity<?> request = new HttpEntity<>(params, headers);
        String url = "http://164.90.230.207:8082/email/check";

        ResponseEntity<?> response = new RestTemplate().postForEntity(url, request, String.class);
        return response.getBody();
    }

    public Object captchaTemplate(CaptchaRequestDto captchaRequestDto) {
        System.out.println("captchatemplate" + captchaRequestDto.toString());
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        Map map = new HashMap<String, String>();

        map.put("Content-Type", "application/json");
        headers.setAll(map);

        Map params = new HashMap();
        params.put("questionId", captchaRequestDto.getQuestionId());
        params.put("answer", captchaRequestDto.getAnswer());

        HttpEntity<?> request = new HttpEntity<>(params, headers);
        String url = "http://164.90.230.207:8081/captcha/answer";

        ResponseEntity<?> response = new RestTemplate().postForEntity(url, request, String.class);
        return captchaRequestDto;
    }

}
